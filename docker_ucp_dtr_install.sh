#!/bin/bash
# UCP install
# swarmmaster IP = 192.168.200.102

# Install UCP

# install UCP on Network interface in Private network
docker container run --rm --name ucp \
  -v /var/run/docker.sock:/var/run/docker.sock \
  docker/ucp:3.2.1 install \
  --host-address 192.168.200.102 \
  --pod-cidr "192.168.0.0/18" \
  --admin-username admin \
  --admin-password adminadmin \
  --license "$(cat /vagrant/docker_subscription.lic)" \
  --force-minimums \
  && echo "Login to https://192.168.200.102 or to https://127.0.0.1:8443/login/. Login admin pass adminadmin"
  
  
# Login to https://192.168.200.102 or to https://127.0.0.1:8443/login/
# UCP login admin
# password adminadmin  